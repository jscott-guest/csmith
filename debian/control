Source: csmith
Maintainer: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Section: devel
Priority: optional
Build-Depends: debhelper (>= 11),
               m4
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/debian/csmith
Vcs-Git: https://salsa.debian.org/debian/csmith.git
Homepage: https://embed.cs.utah.edu/csmith/

Package: csmith
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: generator of random C programs
 Csmith is a tool that can generate random C programs that statically
 and dynamically conform to the C99 standard. Thus, it is useful for
 stress-testing compilers, static analyzers, and other tools that
 process C code.

Package: libcsmith0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: generator of random C programs (runtime library)
 Csmith is a tool that can generate random C programs that statically
 and dynamically conform to the C99 standard. Thus, it is useful for
 stress-testing compilers, static analyzers, and other tools that
 process C code.
 .
 This package contains the runtime library files needed to run software
 using csmith.

Package: libcsmith-dev
Architecture: any
Section: libdevel
Depends: libcsmith0 (= ${binary:Version}),
         ${misc:Depends}
Description: generator of random C programs (development files)
 Csmith is a tool that can generate random C programs that statically
 and dynamically conform to the C99 standard. Thus, it is useful for
 stress-testing compilers, static analyzers, and other tools that
 process C code.
 .
 This package contains the header and development files needed to build
 programs and packages using csmith.
